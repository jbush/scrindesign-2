# About
*Version: 2.1.15. Written by [Joe Bush](http://joebush3d.com).*

Scrindesign 2 is a new and improved way to wrangle [Scrivener](http://www.literatureandlatte.com/scrivener.php"Scrivener") documents into Indesign in an easy, seamless manner.

# Installation
**Step 1:** Drag Scrindesign folder to your [Indesign Scripts Folder](http://indesignsecrets.com/how-to-install-scripts-in-indesign.php)

# Usage
**Optional:** Select a Text Frame

1. Indesign > Window > Utilities > Scripts
2. Scripts > User > Scrindesign > Scrindesign.jsx
3. Edit options
4. Click Convert button

# Features
Scrindesign Conversions can be undone with normal Undo: `Edit > Undo Scrindesign Conversion` and `[ctrl]+z` both work, so don't worry about messing up your manuscript.

Scrindesign assumes a hierarchical manuscript. Be sure to change the options if you are converting a flat manuscript.

Scrindesign will not close if it is not successful.
## Heading Type
### Hierarchical Headings
	1.1. Example Chapter Heading
	13.1.4.2 Another Example Chapter Heading
Compile headings this way using Format As: 

	Non-Fiction with Sub-Heads (Hierarchical)
Use this if your manuscript has any kind of sub-sections.
### Simple Headings
	CHAPTER SEVEN
	The Thing on the Doorstep
Compile headings this way using Format As:

	Novel Standard Manuscript Format
Use this if your manuscript is a flat collection of chapters.
## Heading Options
### Remove Chapter Numbers
For Hierarchical headings. Trims the chapter number off, leaving only the chapter title.
### Remove "Chapter" from Heading
For Simple headings. Trims the word "Chapter" from the heading.
### Combine Chapter Line with Title
Scrivener defaults to the chapter number on one line and the title on the next. This collapses them to a single line.
## Character Styles
### Apply Character Styles
Applies Bold (strong) and Italic (emphasis) character styles to relevant text.
### Convert Underlines to Italics
Dependent on Apply Character Styles

Converts underlined text to italic text. This is standard for novel manuscript submission.
## Misc
### Convert Double Hyphen to Em Dash
Changes the text "--" to "—"
### Collapse Multiple Breaks to Single
Removes extra line breaks, cleaning up your document. It's better to use paragraph spacing anyway.
### Fit Tables to Text Frames
Squeezes any tables into the columns they appear in.
## Convert Buttons
### Convert Longest Story
Finds the longest story in the document and selects its container, then runs the conversion on that.
### Convert Selected Frame
Runs the conversion on the story in the selected frame. It will prompt you to select a frame if you do not have one selected.
# Todo
* [Persistent Options](https://bitbucket.org/jbush/scrindesign-2/issue/5/persistent-options)
* [Add customizable style creation](https://bitbucket.org/jbush/scrindesign-2/issue/3/add-customizable-style-creation)


# Bugs & Feature Requests
Submit bugs and requests here: [Issue tracker at Bitbucket](https://bitbucket.org/jbush/scrindesign-2/issues)

# Credits
* [dispersion design](http://www.dispersiondesign.com/articles/indesign/automatic_styling)
* [indesign secrets](http://indesignsecrets.com/convert-multicolumn-text-frames-to-individual-frames-script.php)
* [Peter Kahrel](http://www.kahrel.plus.com/)
	
