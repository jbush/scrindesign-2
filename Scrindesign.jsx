﻿/* Scrindesign 2.0 
    see readme.md for info.
    */
//#include SID_lib.jsx
#targetengine ScrindesignConversionTool

/* Scrindesign 2.0 
        A script to apply styles to a Scrivener document.
        Special thanks to:
        http://www.dispersiondesign.com/articles/indesign/automatic_styling
        http://indesignsecrets.com/convert-multicolumn-text-frames-to-individual-frames-script.php
        Peter Kahrel
        http://indesignsecrets.com/add-undo-to-your-script.php
    */
var doc = app.activeDocument;
var sel = doc.selection;
var frame = sel[0];
var basicparagraph = doc.paragraphStyles.itemByName("[Basic Paragraph]");
var nonecharacter = doc.characterStyles.itemByName("[None]");
var nonecell = doc.cellStyles.itemByName("[None]");
var basictable = doc.tableStyles.itemByName("[Basic Table]");
var date = new Date();
var now = date.valueOf();

function collapseSimpleHeaders (story) {
    app.findGrepPreferences = NothingEnum.nothing;
    app.changeGrepPreferences = NothingEnum.nothing;
    app.findGrepPreferences.findWhat = "\rCHAPTER .+|\rChapter .+";
    app.changeGrepPreferences.changeTo = "";
    story.changeGrep();
}
function applyParagraphStyle(story, grepStr, styleName, del) {
      app.findGrepPreferences = NothingEnum.nothing;
      app.changeGrepPreferences = NothingEnum.nothing;
      app.findGrepPreferences.findWhat = grepStr;
      var styles = app.activeDocument.paragraphStyles;
      var style;
      try {
            style = styles.item(styleName);
            var n = style.name;
      }
      catch (myError) {
            style = styles.add({
                  name: styleName
            });
      }
      app.changeGrepPreferences.appliedParagraphStyle = style;
      story.changeGrep();
      app.findGrepPreferences.findWhat = grepStr+".*";
      app.changeGrepPreferences.appliedCharacterStyle = nonecharacter;
      story.changeGrep();
      if(del == true){
          // Delete the original match string
          app.findGrepPreferences.findWhat = grepStr;
          app.changeGrepPreferences = NothingEnum.nothing;
          app.changeGrepPreferences.changeTo = "";
      }
      story.changeGrep();
      
}

function applyCharacterStyle(story, grepStr, styleName) {
    app.findGrepPreferences = NothingEnum.nothing;
    app.changeGrepPreferences = NothingEnum.nothing;
    if(grepStr == "Bold") {
        app.findGrepPreferences.fontStyle = grepStr;
    }
    else if (grepStr == "ul") {
        app.findGrepPreferences.underline = true;
    }
    else if(grepStr == "italic") {
        app.findGrepPreferences.fontStyle = grepStr;
    }

    if(grepStr != "clear")  {  
        var styles = app.activeDocument.characterStyles;
        var style;
        try {
            style = styles.item(styleName);
            var n = style.name;
        }
        catch (myError) {
            style = styles.add({
                  name: styleName
            });
        }
        app.changeGrepPreferences.appliedCharacterStyle = style;
        //alert("test");
        story.changeGrep();
        app.changeGrepPreferences = NothingEnum.nothing;
    }
    else {
        app.findGrepPreferences.fontStyle = "";
        app.findGrepPreferences.underline = NothingEnum.nothing;
        app.changeGrepPreferences.appliedCharacterStyle = nonecharacter;
        story.changeGrep();
    }
}
function removeMultiBreaks(story){
    app.findGrepPreferences = NothingEnum.nothing;
    app.changeGrepPreferences = NothingEnum.nothing;
    app.findGrepPreferences.findWhat = "\r{2,}";
    app.changeGrepPreferences.changeTo = "\r";
    story.changeGrep();
}
function fitTables (story) {
	var test = 0;
    var tables = story.tables;
    var style;
    var styleName = "Table Text";
    var styles = app.activeDocument.paragraphStyles;
    try {
        style = styles.item(styleName);
        var n = style.name;
    }
    catch (myError) {
        style = styles.add({
              name: styleName
        });
    }

    for (var i = 0; i < tables.length; i++) {
        test = i;
        var table = tables[i];
        var cells = table.cells;
        //tables.clearTableStyleOverrides();
        table.appliedTableStyle = basictable;
        //Calculate table's width
        var width = "10in";
        var parent = table.parent;
        var bounds = parent.geometricBounds;
        var parentwidth = bounds[3]-bounds[1];
        var columns = parent.textFramePreferences.textColumnCount;
        var gutterwidth = parent.textFramePreferences.textColumnGutter;
        var gutters = columns -1;
        var gutterspace = gutters * gutterwidth;
        width = (parentwidth - gutterspace)/columns;
        table.width = width;
        table.rows[0].rowType = RowTypes.HEADER_ROW;
        
        for(var n = 0; n < cells.length; n++) {
            var cell = cells[n];
            cell.appliedCellStyle = nonecell;
            cell.clearCellStyleOverrides();
            var texts = cell.texts;
            for(var t = 0; t < texts.length; t++) {
                texts[t].appliedParagraphStyle = style;
            }
        }
    }
	//alert(test);
}
function convert(
    w,
    hierChapterRadio,
    simpleChapterRadio,
    removeNumbersCheckbox,
    removeChapterCheckbox,
    combineLinesCheckbox,
    charStylesCheckbox,
    underlineItalicsCheckbox,
    doubleHyphenCheckbox,
    collapseReturnsCheckbox,
    fitTablesCheckbox,
    figuresCheckbox,
    tableCheckbox,
    bulletCheckbox
) {
    doc = app.activeDocument;
    sel = doc.selection;
    frame = sel[0];
    basicparagraph = doc.paragraphStyles.itemByName("[Basic Paragraph]");
    nonecharacter = doc.characterStyles.itemByName("[None]");
    nonecell = doc.cellStyles.itemByName("[None]");
    basictable = doc.tableStyles.itemByName("[Basic Table]");
    date = new Date();
    now = date.valueOf();
    untrimmedHeaders = "Chapter Section Subsection Book Part";
    
    if(frame && frame.constructor.name === "TextFrame") {
        var story = frame.parentStory;
        
        if(collapseReturnsCheckbox) {
            removeMultiBreaks(story);    
        }
        
        if(charStylesCheckbox) {
            applyCharacterStyle(story,"clear","");
            applyCharacterStyle(story,"Bold","Strong");
            applyCharacterStyle(story,"ul","Emphasis");
            //applyCharacterStyle(story,"italic","Emphasis")
            story.appliedParagraphStyle = basicparagraph;
            story.clearOverrides();
        }
        
        if(hierChapterRadio) {
            applyParagraphStyle(story, "Chapter [1-9][0-9]*: ", "Heading 1", removeNumbersCheckbox);
            applyParagraphStyle(story, "^([1-9][0-9]*\.){2} +", "Heading 2", removeNumbersCheckbox);
            applyParagraphStyle(story, "^([1-9][0-9]*\.){3} +", "Heading 3", removeNumbersCheckbox);
            applyParagraphStyle(story, "^([1-9][0-9]*\.){4} +", "Heading 4", removeNumbersCheckbox);
            applyParagraphStyle(story, "^([1-9][0-9]*\.){5} +", "Heading 4", removeNumbersCheckbox);
        }
        else if (simpleChapterRadio) {
            applyParagraphStyle(story, "CHAPTER .+\r.+|Chapter .+\r.+", "Heading 1");
            if(combineLinesCheckbox === true) {
                collapseSimpleHeaders (story);
            }
        }
        if (figuresCheckbox)
            applyParagraphStyle(story, "^Figure [1-9][0-9]*: +", "Figure Title", true);
        if (tableCheckbox)
            applyParagraphStyle(story, "^Table [1-9][0-9]* *", "Table Title", true);
        if (bulletCheckbox)
            applyParagraphStyle(story, "^• +", "Bulleted List", true);
        if(fitTablesCheckbox == true)
            fitTables(story);
            
        //alert("test: "+test);
        
        date = new Date();
        n2 = date.valueOf();
        n3 = (n2-now)/1000;
        //if(n3 >= 5) {
            alert("Scrindesign took " +n3+ " seconds to finish.","Scrindesign Complete");
        //}
        w.close();
    }
    else {
          alert("Please select a text frame containing \
    the story you wish to format");
    }
}

//convert();

/* UI Vars */
//var w = new Window ("dialog", "Scrindesign Conversion Tool");
var w = new Window ("palette", "Scrindesign Conversion Tool");
    var windowTitle = w.add("statictext", undefined, "Scrindesign Conversion Tool");
    //var icon = w.add("image", undefined, "icon.png");
    var optionsGroup = w.add("group");
        optionsGroup.orientation = "row";
        var headingPanel = optionsGroup.add("panel", undefined, "Heading Type");
            headingPanel.alignChildren = "left";
            var hierChapterRadio = headingPanel.add("RadioButton", undefined, "Heirarchical Headings");            
            var simpleChapterRadio = headingPanel.add("RadioButton", undefined, "Simple Headings");
            headingPanel.children[0].value=true;
        var hoptionsPanel = optionsGroup.add("panel", undefined, "Heading Options");
            hoptionsPanel.alignChildren = "left";
            var hPanel = hoptionsPanel.add("group");
                hPanel.orientation = "column";
                var removeNumbersCheckbox = hPanel.add("checkbox", undefined, "Remove Chapter Numbers");
                removeNumbersCheckbox.value = true;
            var sPanel = hoptionsPanel.add("group");
                sPanel.orientation = "column";
                var removeChapterCheckbox = sPanel.add("checkbox", undefined, "Remove \"Chapter\" from heading");
                removeChapterCheckbox.value = true;
                var combineLinesCheckbox = sPanel.add("checkbox", undefined, "Combine Chapter line with Title");
                combineLinesCheckbox.value = true;
    var optionsGroup2 = w.add("group");
        optionsGroup2.orientation = "row";
        var charPanel = optionsGroup2.add("panel", undefined, "Character Styles");
            charPanel.alignChildren = "left";
            var charStylesCheckbox = charPanel.add("checkbox", undefined, "Apply Character Styles");
            charStylesCheckbox.value = true;
            var underlineItalicsCheckbox = charPanel.add("checkbox", undefined, "Convert Underlines to Italics");
            underlineItalicsCheckbox.value = true;
        var miscPanel = optionsGroup2.add("panel", undefined, "Misc");
            miscPanel.alignChildren = "left";
            var doubleHyphenCheckbox = miscPanel.add("checkbox", undefined, "Convert Double Hyphen to Em Dash");
            var collapseReturnsCheckbox = miscPanel.add("checkbox", undefined, "Collapse multiple breaks to single");
            var fitTablesCheckbox = miscPanel.add("checkbox", undefined, "Fit Tables to Text Frames");
            var figuresCheckbox = miscPanel.add("checkbox", undefined, "Style Figure Titles");
            var tableCheckbox = miscPanel.add("checkbox", undefined, "Style Table Titles");
            var bulletCheckbox = miscPanel.add("checkbox", undefined, "Convert bullet characters to List Style");
            
            doubleHyphenCheckbox.value = true;
            collapseReturnsCheckbox.value = true;
            fitTablesCheckbox.value = true;
            figuresCheckbox.value = true;
            tableCheckbox.value = true;
            bulletCheckbox.value = true;
    var submitGroup = w.add("group");
        submitGroup.orientation = "row";
        var submitLButton =submitGroup.add("Button",undefined,"Convert Longest Story");
        var submitSButton =submitGroup.add("Button",undefined,"Convert Selected Frame");
    var ver = w.add("statictext", undefined, "version 2.1.15");
// format nicely
hoptionsPanel.preferredSize = [300,100];
headingPanel.preferredSize = hoptionsPanel.preferredSize;
miscPanel.preferredSize = [300,200];
charPanel.preferredSize = miscPanel.preferredSize;

checkHeadingSet();

function convertUndoable() {
    convert(
        w,
        hierChapterRadio.value,
        simpleChapterRadio.value,
        removeNumbersCheckbox.value,
        removeChapterCheckbox.value,
        combineLinesCheckbox.value,
        charStylesCheckbox.value,
        underlineItalicsCheckbox.value,
        doubleHyphenCheckbox.value,
        collapseReturnsCheckbox.value,
        fitTablesCheckbox.value,
        figuresCheckbox.value,
        tableCheckbox.value,
        bulletCheckbox.value
    );
}
function selectLongestFrame () {
    var stories = app.activeDocument.stories;
    var story = stories[0];
    for (i = 0; i < stories.length; i++) {
            if(stories[i].length > story.length)
                story = stories[i];
    }
    //alert(story.length);
    if(story)
        var selection = story.textContainers[0];
    //alert(story.textContainers.length);
    if(selection)
        app.activeDocument.selection = selection;
}

submitLButton.onClick = function () {
    selectLongestFrame ();
    
    if (parseFloat(app.version) < 6) {
        convertUndoable();
    }
    else {
        app.doScript(convertUndoable, ScriptLanguage.JAVASCRIPT, undefined, UndoModes.ENTIRE_SCRIPT, "Scrindesign Conversion");
    }
    //w.close();
}

submitSButton.onClick = function () {

    if (parseFloat(app.version) < 6) {
        convertUndoable();
    }
    else {
        app.doScript(convertUndoable, ScriptLanguage.JAVASCRIPT, undefined, UndoModes.ENTIRE_SCRIPT, "Scrindesign Conversion");
    }
    //w.close();
}

function checkHeadingSet(){
    if(simpleChapterRadio.value == true){
        sPanel.enabled = true;
        hPanel.enabled = false;
    }
    if(hierChapterRadio.value == true) {
        sPanel.enabled = false;
        hPanel.enabled = true;
    }
}
simpleChapterRadio.onClick = function () {checkHeadingSet()}
hierChapterRadio.onClick = function () {checkHeadingSet()}

w.show();